PROJET BLOG : Next.js et Symfony.

Maquettes et diagramme de classe dans le dossier conception. 

Projet symfony : https://gitlab.com/lauragotti1/blog

**Back :** 

Juste une classe/repo/controller pour la table Article. 
Table pour les tags et table de jointure créees mais pas utilisées.

CRUD (get all/id, post, delete et put) + requete de recherche dans le titre.

**Front :**

Style avec css, bootstrap et react bootstrap(que j'ai du installer à la fin pour que les modales fonctionnent)

Fonctionnalités front:

- consulter la liste de preview de tous les articles sur la page d'accueil (dans l'ordre du plus récent au plus ancien)

- consulter un article en entier(titre, auteur, date publication, image, texte et lien playlist spotify)

- modifier un article, ouvre en dessous de l'article un formulaire de modification

- supprimer un article (ouvre une modale de confirmation)

- barre de recherche dans la nav, recherche uniquement dans les titres d'articles puis affiche les resultats dans une modale



**A faire :**

- gestion des tags/catégories, dans le back et le front

- reset la valeur dans la barre de recherche 

- ajouter de la validation dans le back
