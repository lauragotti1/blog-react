import { postArticle } from "@/article-service";
import { ArticleI } from "@/entities";
import { useRouter } from "next/router";
import { FormEvent, useState } from "react";

interface Props {
    onsubmit:(article:ArticleI)=>void;
    edited?:ArticleI;
}

export default function ArticleForm({onsubmit, edited}:Props) {
const [errors, setErrors] = useState('');

const [article, setArticle] = useState<ArticleI>(edited? edited : {
    title:'',
    cover:'',
    author:'',
    text:'',
    spotify:''
});

function handleChange(event:any) {
    setArticle({
        ...article,
        [event.target.name]: event.target.value
    });
}

async function handleSumbit(event:FormEvent) {
    event.preventDefault();
    try {
        onsubmit(article);
    } catch (error:any) {
        if(error.response.status == 400) {
            setErrors(error.response.data.detail);
        }
    }
}

    return (
        <>
            
                    <form onSubmit={handleSumbit}> 
                    {errors && <p>{errors}</p>}
                        <div className="mb-3">
                            <label htmlFor="title" className="form-label">Titre :</label>
                            <input type="text" className="form-control" name="title" id="title" aria-describedby="title" value={article.title} onChange={handleChange} required/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="cover" className="form-label">Lien image :</label>
                            <input type="text" className="form-control" name="cover" id="cover" value={article.cover} onChange={handleChange}/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="author" className="form-label">Auteur/Autrice :</label>
                            <input type="text" className="form-control" name="author" id="author" value={article.author} onChange={handleChange} required/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="text" className="form-label">Article :</label>
                            <textarea className="form-control" name="text" id="text" value={article.text} onChange={handleChange} maxLength={2000}/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="spotify" className="form-label">Lien embed spotify :</label>
                            <input type="text" className="form-control" name="spotify" id="spotify" value={article.spotify} onChange={handleChange}/>
                        </div>
                        <button type="submit" className="btn btn-success">Envoyer</button>
                    </form>
               
        </>
    )
}
