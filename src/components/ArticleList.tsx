import { fetchAllArticles } from "@/article-service";
import { ArticleI } from "@/entities";
import Link from "next/link";
import { useEffect, useState } from "react";

export default function ArticleList() {
    const [articles, setArticles] = useState<ArticleI[]>([])

    useEffect(() => {
        fetchAllArticles().then(data => {
            sortDesc(data)
            setArticles(data);
        });
    }, [])

    function sortDesc(tab:any) {
        tab.sort((a:any, b:any) => {
            return b.id - a.id;
          });
    }

    return (
        <>
            <div className="row justify-content-center article-list-main">
                <div className="col-10 col-md-8">
                    <div className="row">
                        <h1>Derniers Articles</h1>
                        
                    </div>
                    <div className="row cards-row">
                    {articles.map((item) => 
                    
                    <div className="card m-1 col-md-5" key={item.id}>
                        <div className="row">
                            <div className="col-4">
                                <img src={item.cover} className="img-fluid" alt="album cover" />
                            </div>
                            <div className="col-8">
                                <div className="card-body">
                                    <h5 className="card-title">{item.title}</h5>
                                    <p className="card-text"><Link href={"/article/"+item.id}>Lire l'article</Link></p>
                                </div>
                            </div>
                        </div>
                    </div>
             

                    )}
                    </div>

                    
                </div>
            </div>

        </>
    );

}