import { searchTitle } from "@/article-service";
import Link from "next/link";
import { useRouter } from "next/router";
import { FormEvent, useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button, Modal } from "react-bootstrap";
import { ArticleI } from "@/entities";

export default function Nav() {
  const [noError, setNoError] = useState(true);
  const [result, setResult] = useState<ArticleI[]>([]);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

const [title, setTitle] = useState({title:""});

function handleChange(event:any) {
  setTitle({
    ...title,
    [event.target.name]:event.target.value
  })
}

async function handleSubmit(event:FormEvent) {
  event.preventDefault();
  const searched = await searchTitle(title.title)
  .catch(error => {
    console.log(error);
    if(error.response.status == 404) {
        setNoError(false);
        // router.push('/404');
    }
}) 
setResult(searched);
}

const [show, setShow] = useState(false);

  return (
    <>
    <nav className="custom-navbar navbar bg-dark navbar-expand-md bg-body-tertiary" data-bs-theme="dark">
      <div className="container-fluid">
        <Link className="navbar-brand" href="/">
        <img src="/favicon.ico" className="img-fluid"/>Blog</Link>

        <button className="navbar-toggler navbar-dark" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse justify-content-between" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <Link className="nav-link" aria-current="page" href="/">Accueil</Link>
            </li>

            <li className="nav-item">
              <Link className="nav-link" href="/add">Nouvel article</Link>
            </li>
            

          </ul>
          <form className="d-flex search" role="search" onSubmit={handleSubmit}>
              <input className="form-control me-2" type="search" placeholder="Rechercher" aria-label="Rechercher" name="title" value={title.title} onChange={handleChange}/>
              <button className="btn btn-outline-success" type="submit" onClick={handleShow}>Rechercher</button>
            </form>
        </div>
      </div>
    </nav>

    <Modal show={show} onHide={handleClose} className="modal">
        <Modal.Header closeButton>
          <Modal.Title>Résultats de la recherche "{title.title}"</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {noError ? result.map((item) => <p key={item.id}><Link href={"/article/"+item.id} onClick={handleClose}>{item.title}</Link></p>) : <p>Pas de résultats</p>}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Fermer
          </Button>
        </Modal.Footer>
      </Modal>

    
    </>
    

    
  );

}