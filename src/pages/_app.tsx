import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";


import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { useEffect } from "react";
import Nav from "@/components/Nav";
import Footer from "@/components/Footer";
import Head from "next/head";

export default function App({ Component, pageProps }: AppProps) {
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");
  }, []);
  return (
    <>
    <Head>
      <title>Blog</title>
    </Head>
    <Nav/>
  <Component {...pageProps} />
    {/* <Footer/> */}
  </>
  )

}
