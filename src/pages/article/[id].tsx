import { deleteArticle, fetchOneArticle, updateArticle } from "@/article-service";
import ArticleForm from "@/components/ArticleForm";
import { ArticleI } from "@/entities";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";

export default function ArticleId() {
    const router = useRouter();
    const { id } = router.query;
    const [article, setArticle] = useState<ArticleI>();
    const [showEdit, setShowEdit] = useState(false);


    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
  

    useEffect(() => {
        if (!id) {
            return;
        }
        fetchOneArticle(Number(id))
            .then(data => setArticle(data))
            .catch(error => {
                console.log(error);
                if (error.response.status == 404) {
                    router.push('/404')
                }
            });
    }, [id])

    async function update(article:ArticleI) {
        const updated = await updateArticle(article);
        setShowEdit(!showEdit)
        setArticle(updated)
    }

    async function remove() {
        await deleteArticle(id);
        router.push('/');
    }

    if (!article) {
        return <p>Loading...</p>
    }
    
    return (
        <div className="article-main row justify-content-center">
            <div className="col-10 col-md-8">
                <div className="row">
                    <div className="col-md-8 header-text">
                        <h2>{article?.title}</h2>
                        <p>publié le {article.date && new Date(article.date).toLocaleDateString()} par {article.author}</p>
                    </div>
                    <div className="col-md-4">
                        <img src={article.cover} alt="album-cover" className="img-fluid" />
                    </div>
                    <div className="col-12 article-text">
                        <p>{article?.text}</p>
                    </div>
                    <div className="col-12 spotify-embed">
                        <div className="spotify-div"><iframe src={article.spotify} className="spotify-iframe" allow="clipboard-write; encrypted-media; fullscreen; picture-in-picture;"></iframe></div>
                    </div>

                    <div className="col-12">
                        <div className="col-12 line"></div>
                        <div className="col-12 article-bottom">
                            <p><button className="articlebtn" onClick={() => setShowEdit(!showEdit)}><a href="#modifier">MODIFIER</a></button> <button className="articlebtn" onClick={handleShow}>SUPPRIMER</button></p>
                        </div>

                    </div>
                </div>
                {showEdit &&    
                    <div className="row">
                        <div className="col-12">
                        <div className="col-12 line"></div>
                        <h2 id="modifier">Modifier</h2>
                        <div>
                            <ArticleForm edited={article} onsubmit={update} />
                        </div>
                        </div>
                    </div>}

            </div>

            <Modal show={show} onHide={handleClose} className="modal">
        <Modal.Header closeButton>
          
        </Modal.Header>
        <Modal.Body>
        Êtes vous sûr de vouloir supprimer?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
           Annuler
          </Button>
          <Button variant="danger" onClick={remove}>
            Supprimer
          </Button>
        </Modal.Footer>
      </Modal>
      
        </div>
    )
}