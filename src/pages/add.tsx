import { postArticle } from "@/article-service";
import ArticleForm from "@/components/ArticleForm";
import { ArticleI } from "@/entities";
import { useRouter } from "next/router";

export default function Add() {
    const router = useRouter();

    async function addArticle(article: ArticleI) {
        const added = await postArticle(article);
        router.push('/article/'+added.id)
    }

    return (
        <>
            <div className="form-main row justify-content-center">
                <div className="col-10 col-md-8">
                    <div className="row"></div>
                    <h1 className="col-10 col-md-8">Nouvel Article</h1>
                    <ArticleForm onsubmit={addArticle} />
                </div>
            </div>
        </>
    )
}