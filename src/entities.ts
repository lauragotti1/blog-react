export interface ArticleI {
    id?:number,
    title?:string,
    date?:string,
    author?:string,
    cover?:string,
    text?:string,
    spotify?:string
}