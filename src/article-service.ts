import axios from "axios";
import { ArticleI } from "./entities";

export async function fetchAllArticles () {
    const response = await axios.get<ArticleI[]>('http://localhost:8000/api/article');
    return response.data;
}

export async function fetchOneArticle (id:number) {
    const response = await axios.get<ArticleI>('http://localhost:8000/api/article/'+id);
    return response.data;
}

export async function postArticle(article:ArticleI) {
    const response = await axios.post<ArticleI>('http://localhost:8000/api/article', article);
    return response.data;
}

export async function updateArticle(article:ArticleI) {
    const response = await axios.put<ArticleI>('http://localhost:8000/api/article', article);
    return response.data;
}

export async function deleteArticle(id:any) {
    await axios.delete('http://localhost:8000/api/article/'+id);
}

export async function searchTitle(title:string) {
    const response = await axios.get('http://localhost:8000/api/article/search/'+title);
    return response.data;
}